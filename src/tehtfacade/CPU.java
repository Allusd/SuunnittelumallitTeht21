/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtfacade;

/**
 *
 * @author aleks
 */
public class CPU {
    public void freeze() { 
        System.out.println("Frozen");
        
    }
    public void jump(long position) { 
        long pos = position+1;
        System.out.println("Jumping to " + pos);
    }
    public void execute() {
        System.out.println("Executing");
    }
    
}
