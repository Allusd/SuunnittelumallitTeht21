/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtfacade;

/**
 *
 * @author aleks
 */
public class ComputerFacade {
    private CPU processor;
    private Memory ram;
    private HardDrive hd;
    Long BOOT_ADDRESS = 100L;
    Long BOOT_SECTOR = 1L;
    Byte SECTOR_SIZE = 100;

    public ComputerFacade() {
        this.processor = new CPU();
        this.ram = new Memory();
        this.hd = new HardDrive();
    }

    public void start() {
        processor.freeze();
        ram.load(BOOT_ADDRESS, hd.read(BOOT_SECTOR, SECTOR_SIZE));
        processor.jump(BOOT_ADDRESS);
        processor.execute();
    }
}